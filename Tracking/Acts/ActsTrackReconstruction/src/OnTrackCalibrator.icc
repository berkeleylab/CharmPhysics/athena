/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef ONTRACKCALIBRATOR_ICC
#define ONTRACKCALIBRATOR_ICC

#include "ActsGeometry/SurfaceOfMeasurementUtil.h"

namespace ActsTrk {

template <typename traj_t>
OnTrackCalibrator<traj_t> OnTrackCalibrator<traj_t>::NoCalibration(const Acts::TrackingGeometry &trackingGeometry,
                                                                   const ActsTrk::DetectorElementToActsGeometryIdMap &detectorElementToGeoId)
{
    ToolHandle<IOnTrackCalibratorTool<traj_t>> null(nullptr);
    return OnTrackCalibrator(trackingGeometry, detectorElementToGeoId, null, null);
}

template <typename traj_t>
OnTrackCalibrator<traj_t>::OnTrackCalibrator(const Acts::TrackingGeometry &trackingGeometry,
                                             const ActsTrk::DetectorElementToActsGeometryIdMap &detectorElementToGeoId,
                                             const ToolHandle<IOnTrackCalibratorTool<traj_t>>& pixelTool,
                                             const ToolHandle<IOnTrackCalibratorTool<traj_t>>& stripTool)
   : m_trackingGeometry(&trackingGeometry),
     m_detectorElementToGeoId(&detectorElementToGeoId)
{
    if (pixelTool.isEnabled()) {
	pixelTool->connect(*this);
    } else {
	pixel_calibrator.template connect<&OnTrackCalibrator<traj_t>::passthrough<2, xAOD::PixelCluster>>(this);
    }

    if (stripTool.isEnabled()) {
	stripTool->connect(*this);
    } else {
        // cppcheck-suppress missingReturn; false positive
	strip_calibrator.template connect<&OnTrackCalibrator<traj_t>::passthrough<1, xAOD::StripCluster>>(this);
    }
}

template <typename traj_t>
void OnTrackCalibrator<traj_t>::calibrate(
    const Acts::GeometryContext& geoctx,
    const Acts::CalibrationContext& cctx,
    const Acts::SourceLink& link,
    TrackStateProxy state) const
{
    state.setUncalibratedSourceLink(link);
    ATLASUncalibSourceLink sourceLink = link.template get<ATLASUncalibSourceLink>();
    assert(sourceLink!=nullptr);
    const xAOD::UncalibratedMeasurement &measurement = getUncalibratedMeasurement(sourceLink);
    // @TODO in principle only need to lookup surface for strips
    const Acts::Surface *surface = getSurfaceOfMeasurement(*m_trackingGeometry, *m_detectorElementToGeoId, measurement);
    if (!surface) throw std::runtime_error("No surface for measurement.");
    switch (measurement.type()) {
    case xAOD::UncalibMeasType::PixelClusterType: {
	assert(pixel_calibrator.connected());
	auto [pos, cov] = pixel_calibrator(
	    geoctx,
	    cctx,
	    *dynamic_cast<const xAOD::PixelCluster*>(&measurement),
	    state);
	setState<2>(
	    xAOD::UncalibMeasType::PixelClusterType,
	    pos,
	    cov,
	    surface->bounds().type(),
	    state);
	break;
    }
    case xAOD::UncalibMeasType::StripClusterType: {
	assert(strip_calibrator.connected());
	auto [pos, cov] = strip_calibrator(
	    geoctx,
	    cctx,
	    *dynamic_cast<const xAOD::StripCluster*>(&measurement),
	    state);
	setState<1>(
	    xAOD::UncalibMeasType::StripClusterType,
	    pos,
	    cov,
	    surface->bounds().type(),
	    state);
	break;
    }
    default:
	throw std::domain_error("OnTrackCalibrator can only handle Pixel or Strip measurements");
    }
}

template <typename traj_t>
template <std::size_t Dim, typename Cluster>
std::pair<xAOD::MeasVector<Dim>, xAOD::MeasMatrix<Dim>>
OnTrackCalibrator<traj_t>::passthrough(
    const Acts::GeometryContext& /*gctx*/,
	    const Acts::CalibrationContext& /*cctx*/,
	    const Cluster& cluster,
	    const TrackStateProxy& /*state*/) const
{
    return std::make_pair(cluster.template localPosition<Dim>(),
			  cluster.template localCovariance<Dim>());
}


} // namespace ActsTrk

#endif
